const http = require('http')
const app = require('./api/app')
const db = require('./api/helpers/connection')

const port = 3000

try {
    
    db.connect(); 
    const server = http.createServer(app) 
    server.listen(port)
    console.log('Running on http://localhost:3000') 
    
} catch (error) {
    console.log(error)
}

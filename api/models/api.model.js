// Users controller
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const ObjectId = require('mongodb').ObjectId
const db = require('../helpers/connection')
 

exports.getTopic = async function() {
    
  let data;
  let status = false;

  var pipeline = [ 
    {
      '$sort' : {
        '_id' : 1
      }
    }
  ];

  try { 
    await db.getdb().collection('topics')
      .aggregate(pipeline)  
      .toArray()
      .then((res) => {
        status = true;
        data = res; 
      })
      .catch((error) => {
        status = false; 
        data = error;
      }) 
    
    return {
      result_status: status,
      result_data: data
    };

  } 
  catch (error) 
  {
    return {
      result_status: status,
      result_data: error
    };
  }
 
}


exports.getSampleTweet = async function(size, keyword) {
    
  let data;
  let status = false;

  var pipeline = [
    
    { '$sample': 
      {
        size: size
      } 
    },
    { '$match': 
      { 
        scoring_progress: true, 
        search_value: keyword 
      } 
    },
    { '$project':
      { 
        '_id': 1,
        'mongo_created_at': 1,
        'id': 1,
        'created_at': 1,
        'text': 1,
        'user': 1,
        'entities': 1,
        'search_by': 1,
        'search_value': 1,
        'scoring_progress': 1,
        'scoring_point': 1,
        'scoring_sentiment': 1,
        'scoring_progress_date': 1 
      } 
    },
    {
      '$sort' : {
        'id' : 1
      }
    }
  ];


  try {
     
    await db.getdb().collection('tweets')
      .aggregate(pipeline)  
      .toArray()
      .then((res) => {
        status = true;
        data = res; 
      })
      .catch((error) => {
        status = false; 
        data = error;
      }) 
    
    return {
      result_status: status,
      result_data: data
    };

  } 
  catch (error) 
  {
    return {
      result_status: status,
      result_data: error
    };
  }
 
}


 
 
const MongoClient = require('mongodb').MongoClient
const assert = require('assert'); 

const url = 'mongodb://admin:password@35.198.216.228:27017/admin' 
// const url = 'mongodb://localhost:27017/SentimentAnalysis' 
// Database Name
const dbName = 'SentimentAnalysis'
// state contains the object where the database can be updated
const state = {
  db: null
}

// Use connect method to connect to the server (Promise)
exports.connect = () => {

    MongoClient.connect(url, { useNewUrlParser: true }, function(err, client) {  
      if (err) {
        state.db = null; 
        console.log(err)
        return console.log(err);
      }
      console.log('Connected to database')
      console.log() 
      state.db = client.db(dbName)  
      return client.db(dbName);
    });
   
}

exports.getdb = () => {   
  return state.db;  
}

exports.close = (done) => {
  if (state.db) {
    state.db.close((err, result) => {
      state.db = null
      state.mode = null
      done(err)
    })
  }
}

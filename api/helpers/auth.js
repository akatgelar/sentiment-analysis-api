const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  try {
    // Token is sent as "Bearer Token" so needs to be split
    const token = req.headers.authorization.split(' ')[1]
    // Token is decoded to reveal user data
    const decoded = jwt.verify(token, 'secretkey')
    // Decoded user data can be accessed by controllers
    req.userData = decoded
    next()
  } catch (error) {
    // On failiure to authenticate user,
    // Controller method cannot be accessed
    return res.status(401).json({
      message: 'Auth failed'
    })
  }
}

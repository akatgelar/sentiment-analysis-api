exports.sortByDateAsc = (lhs, rhs) => { 
    return lhs > rhs ? 1 : lhs < rhs ? -1 : 0;
}

exports.sortByDateDesc = (lhs, rhs) => {
    return lhs < rhs ? 1 : lhs > rhs ? -1 : 0;
}

exports.sortByTimeAsc = (lhs, rhs) => {
    var results;
  
    results = lhs.hours() > rhs.hours() ? 1 : lhs.hours() < rhs.hours() ? -1 : 0;
  
    if (results === 0) results = lhs.minutes() > rhs.minutes() ? 1 : lhs.minutes() < rhs.minutes() ? -1 : 0;
  
    if (results === 0) results = lhs.seconds() > rhs.seconds() ? 1 : lhs.seconds() < rhs.seconds() ? -1 : 0;
  
    return results;
}

exports.sortByTimeDesc = (lhs, rhs) => {
    var results;
    
    results = lhs.hours() < rhs.hours() ? 1 : lhs.hours() > rhs.hours() ? -1 : 0;
  
    if (results === 0) results = lhs.minutes() < rhs.minutes() ? 1 : lhs.minutes() > rhs.minutes() ? -1 : 0;
  
    if (results === 0) results = lhs.seconds() < rhs.seconds() ? 1 : lhs.seconds() > rhs.seconds() ? -1 : 0;
  
    return results;
}

exports.sortByDateAscAndTimeAsc = (lhs, rhs) => {
    var results;
  
    results = lhs.year() > rhs.year() ? 1 : lhs.year() < rhs.year() ? -1 : 0;
  
    if (results === 0) results = lhs.month() > rhs.month() ? 1 : lhs.month() < rhs.month() ? -1 : 0;
  
    if (results === 0) results = lhs.date() > rhs.date() ? 1 : lhs.date() < rhs.date() ? -1 : 0;
  
    if (results === 0) results = lhs.hours() > rhs.hours() ? 1 : lhs.hours() < rhs.hours() ? -1 : 0;
  
    if (results === 0) results = lhs.minutes() > rhs.minutes() ? 1 : lhs.minutes() < rhs.minutes() ? -1 : 0;
  
    if (results === 0) results = lhs.seconds() > rhs.seconds() ? 1 : lhs.seconds() < rhs.seconds() ? -1 : 0;
  
    return results;
}

exports.sortByDateDescAndTimeDesc = (lhs, rhs) => {
    var results;
  
    results = lhs.year() < rhs.year() ? 1 : lhs.year() > rhs.year() ? -1 : 0;
  
    if (results === 0) results = lhs.month() < rhs.month() ? 1 : lhs.month() > rhs.month() ? -1 : 0;
  
    if (results === 0) results = lhs.date() < rhs.date() ? 1 : lhs.date() > rhs.date() ? -1 : 0;
  
    if (results === 0) results = lhs.hours() < rhs.hours() ? 1 : lhs.hours() > rhs.hours() ? -1 : 0;
  
    if (results === 0) results = lhs.minutes() < rhs.minutes() ? 1 : lhs.minutes() > rhs.minutes() ? -1 : 0;
  
    if (results === 0) results = lhs.seconds() < rhs.seconds() ? 1 : lhs.seconds() > rhs.seconds() ? -1 : 0;
  
    return results;
}
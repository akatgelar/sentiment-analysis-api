const express = require('express')
const router = express.Router()

const checkAuth = require('../helpers/auth') 
const UserController = require('../controllers/users.controller')

router.get('/', 
    checkAuth, 
    UserController.user_get_all)

router.post('/signup', 
    UserController.user_signup)

router.post('/login', 
    UserController.user_login)
 
router.delete('/:userId', 
    checkAuth, 
    UserController.user_delete)

module.exports = router

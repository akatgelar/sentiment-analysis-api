const express = require("express");
const router = express.Router();

const Auth = require("../helpers/auth"); 
const ApiController = require("../controllers/api.controller");  

/* GET root. */ 
router.get('/', function (req, res) {
  var newObj =  { 
    'data' : 'Welcome to sentiment analysis api.'
  };

  res.json({ code : 200, success: true,  message: "OK", data: newObj });
})
   
module.exports = router;

const express = require("express");
const router = express.Router();

const Auth = require("../helpers/auth"); 
const ApiController = require("../controllers/api.controller");  

/* GET root. */
router.get("/", 
  // Auth, 
  ApiController.index);
 
router.get("/getTotalTweet", 
  // Auth, 
  ApiController.getTotalTweet);
 
router.get("/getTotalProgress", 
  // Auth, 
  ApiController.getTotalProgress);
 
router.get("/getTotalSentiment", 
  // Auth, 
  ApiController.getTotalSentiment);
 
router.get("/getSample2019GantiPresiden", 
  // Auth, 
  ApiController.getSample2019GantiPresiden);
   
router.get("/getSample2019TetapJokowi", 
  // Auth, 
  ApiController.getSample2019TetapJokowi);
   
router.get("/getSampleJokowi", 
  // Auth, 
  ApiController.getSampleJokowi);
   
router.get("/getSamplePrabowo", 
  // Auth, 
  ApiController.getSamplePrabowo);
   
module.exports = router;

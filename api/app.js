const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const favicon = require('serve-favicon')
const path = require('path')
const db = require('./helpers/connection')

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
    return res.status(200).json({})
  }
  next()
})

// app.use('/uploads', express.static('uploads'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(morgan('dev'))
app.use(helmet())
 


const indexRoutes = require('./routes/index.router')
const userRoutes = require('./routes/users.router')
const apiRoutes = require('./routes/api.router')  



app.use('/', indexRoutes)
app.use('/users', userRoutes)
app.use('/api', apiRoutes)  
 

app.use((req, res, next) => { 
    var err = new Error("Not Found");
    err.status = 404;
    res.status(err.status || 404);
    res.json({ code : 404, status: "404 Not Found ", message: err.message });
})

// API err
app.use((err, req, res, next) => {
    // var err = new Error("Not Found");
    err.status = 500;
    res.status(err.status || 500);
    res.json({ code : "500", status: "500 Internal Server Error", message: err.message });
})

module.exports = app

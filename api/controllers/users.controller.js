// Users controller
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const ObjectId = require('mongodb').ObjectId
const db = require('../helpers/connection')

// The collection being used
const dbCol = 'users'

exports.user_get_all = (req, res, next) => {
  // Get all users 
  const collection = db.getdb().collection(dbCol)
  collection.find({}) 
    .toArray() 
    .then(docs => {
      res.status(200).json({
        results: docs
      })
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
}

exports.user_signup = (req, res, next) => {
  // Create a new user
  const collection = db.getdb().collection(dbCol)
  collection
    .findOne({ email: req.body.email })
    .then(docs => {
      if (!docs) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            })
          } else {
            const user = {
              email: req.body.email,
              password: hash,
              password_plain:req.body.password
            }
            const collection = db.getdb().collection(dbCol)
            collection.insertOne(user)
              .then(docs => {
                res.status(200).json({
                  message: 'User created',
                  docs
                })
              })
          }
        })
      } else {
        return res.status(409).json({
          message: 'Mail exists'
        })
      }
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
}

exports.user_login = (req, res, next) => {
  // User login
  const collection = db.getdb().collection(dbCol)
  collection
    .findOne({ email: req.body.email })
    .then(docs => {
      if (!docs) {
        return res.status(401).json({
          message: 'Auth failed 1'
        })
      } else {
        bcrypt.compare(req.body.password, docs.password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: 'Auth failed'
            })
          }
          if (result) {
            let token = jwt.sign({
              email: docs.email,
              userId: docs._id
            }, 'secretkey', {expiresIn: '1d'})
            return res.status(200).json({
              message: 'Auth successful',
              token: token
            })
          }
          res.status(401).json({
            message: 'Auth failed 2'
          })
        })
      }
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
}



exports.user_delete = (req, res, next) => {
  // Delete a user
  const id = req.params.userId
  const collection = db.getdb().collection(dbCol)
  collection
    .deleteOne({ _id: ObjectId(id) })
    .then(docs => {
      // No user found with this id
      if (docs.result.n < 1) {
        res.status(404).json({
          message: 'User not found',
          result: docs.result
        })
      } else {
        res.status(200).json({
          message: 'User deleted',
          result: docs.result
        })
      }
    })
    .catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
}

const ApiModel = require('../models/api.model') 
const Moment = require('moment');
const { extendMoment } = require('moment-range');
const moment = extendMoment(Moment);

const { sortByDateAscAndTimeAsc, sortByDateDescAndTimeDesc } = require('../helpers/helper');
  
exports.index = async (request, result, next) => {
  
  var newObj =  { 
    'data' : 'Welcome to sentiment analysis api.'
  };

  return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });

}

exports.getTotalTweet = async (request, result, next) => {
  
  try {
    var tweets = await ApiModel.getTopic();   
    var newObj =  
    {
      'total_tweet' : 0,
      'first_tweet' : "",
      'last_tweet' : "",
      'range' : "",
      'tweet_per_day' : "",
      'topics' : []
    }

    var date_asc = [];
    var date_desc = [];
    
    // sum total
    for (index = 0; index < tweets.result_data.length; ++index) {  
      newObj.total_tweet = newObj.total_tweet + Number(tweets.result_data[index]['count_all']);  
    }

    // each hashtag
    for (index = 0; index < tweets.result_data.length; ++index) {  
      var date_asc_temp = moment(tweets.result_data[index]['created_at_asc'], "ddd MMM DD HH:mm:ss Z YYYY"); 
      date_asc.push(date_asc_temp);
      
      var date_desc_temp = moment(tweets.result_data[index]['created_at_desc'], "ddd MMM DD HH:mm:ss Z YYYY"); 
      date_desc.push(date_desc_temp);

      var temp = {
        'search_by' : tweets.result_data[index]['type'],
        'search_value' : tweets.result_data[index]['value'],
        'count_all' : tweets.result_data[index]['count_all'],
        'percentage' : ((tweets.result_data[index]['count_all'] / newObj.total_tweet) * 100).toFixed(2),
      };
      newObj.topics.push(temp);
    }
  
    // sorting date to find first and last
    date_asc.sort(sortByDateAscAndTimeAsc); 
    date_desc.sort(sortByDateDescAndTimeDesc); 

    // get duration day
    var duration = moment().range(date_asc[0], date_desc[0]);
    var range = duration.diff('days');
 
    // get tweet per day
    var tweet_per_day = Math.floor(newObj.total_tweet / range);

    // fill the public object
    newObj.first_tweet = date_asc[0];
    newObj.last_tweet = date_desc[0];
    newObj.range = range;
    newObj.tweet_per_day = tweet_per_day;
 
    if (tweets.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: tweets.result_data });
    }

  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message }); 
  }
  
 

  
    
}
 

exports.getTotalProgress = async (request, result, next) => {
  
  try {
    var progress = await ApiModel.getTopic();   
    var newObj =  
    {
      'count_all' : 0,
      'count_finish' : 0,
      'count_unfinish' : 0,
      'percentage_finish' : 0,
      'percentage_unfinish' : 0,
      'topics' : []
    }
  
    // sum total
    for (index = 0; index < progress.result_data.length; ++index) {  
      newObj.count_all = newObj.count_all + Number(progress.result_data[index]['count_all']);  
      newObj.count_finish = newObj.count_finish + Number(progress.result_data[index]['count_finish']);  
      newObj.count_unfinish = newObj.count_unfinish + Number(progress.result_data[index]['count_unfinish']);  
    }

    // percentage all
    newObj.percentage_finish = ((newObj.count_finish /  (newObj.count_finish+newObj.count_unfinish)) * 100).toFixed(2);
    newObj.percentage_unfinish = ((newObj.count_unfinish /  (newObj.count_finish+newObj.count_unfinish)) * 100).toFixed(2);

    // each hashtag
    for (index = 0; index < progress.result_data.length; ++index) {   
      var temp = {
        'search_by' : progress.result_data[index]['type'],
        'search_value' : progress.result_data[index]['value'],
        'count_all' : progress.result_data[index]['count_all'],
        'count_finish' : progress.result_data[index]['count_finish'],
        'count_unfinish' : progress.result_data[index]['count_unfinish'],
        'percentage_finish' : ((progress.result_data[index]['count_finish'] / (progress.result_data[index]['count_finish']+progress.result_data[index]['count_unfinish'])) * 100).toFixed(2),
        'percentage_unfinish' : ((progress.result_data[index]['count_unfinish'] / (progress.result_data[index]['count_finish']+progress.result_data[index]['count_unfinish'])) * 100).toFixed(2)
      };
      newObj.topics.push(temp);
    }
    
    if (progress.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: progress.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
  
    
}


exports.getTotalSentiment = async (request, result, next) => {
  
  try {
    var progress = await ApiModel.getTopic();   
    var newObj =  
    {
      'count_all' : 0,
      'count_finish' : 0,
      'sentiment_positif' : 0,
      'sentiment_netral' : 0,
      'sentiment_negatif' : 0,
      'percentage_positif' : 0,
      'percentage_netral' : 0,
      'percentage_negatif' : 0,
      'topics' : []
    }
  
    // sum total
    for (index = 0; index < progress.result_data.length; ++index) {  
      newObj.count_all = newObj.count_all + Number(progress.result_data[index]['count_all']);  
      newObj.count_finish = newObj.count_finish + Number(progress.result_data[index]['count_finish']);  
      newObj.sentiment_positif = newObj.sentiment_positif + Number(progress.result_data[index]['sentiment_positif']);  
      newObj.sentiment_netral = newObj.sentiment_netral + Number(progress.result_data[index]['sentiment_netral']);  
      newObj.sentiment_negatif = newObj.sentiment_negatif + Number(progress.result_data[index]['sentiment_negatif']);  
    }

    // percentage all
    newObj.percentage_positif = ((newObj.sentiment_positif /  (newObj.sentiment_positif+newObj.sentiment_netral+newObj.sentiment_negatif)) * 100).toFixed(2);
    newObj.percentage_netral = ((newObj.sentiment_netral /  (newObj.sentiment_positif+newObj.sentiment_netral+newObj.sentiment_negatif)) * 100).toFixed(2);
    newObj.percentage_negatif = ((newObj.sentiment_negatif /  (newObj.sentiment_positif+newObj.sentiment_netral+newObj.sentiment_negatif)) * 100).toFixed(2);

    // each hashtag
    for (index = 0; index < progress.result_data.length; ++index) {   
      var temp = {
        'search_by' : progress.result_data[index]['type'],
        'search_value' : progress.result_data[index]['value'],
        'count_all' : progress.result_data[index]['count_all'],
        'count_finish' : progress.result_data[index]['count_finish'],
        'sentiment_positif' : progress.result_data[index]['sentiment_positif'],
        'sentiment_netral' : progress.result_data[index]['sentiment_netral'],
        'sentiment_negatif' : progress.result_data[index]['sentiment_negatif'],
        'percentage_positif' : ((progress.result_data[index]['sentiment_positif'] / (progress.result_data[index]['sentiment_positif']+progress.result_data[index]['sentiment_netral']+progress.result_data[index]['sentiment_negatif'])) * 100).toFixed(2),
        'percentage_netral' : ((progress.result_data[index]['sentiment_netral'] / (progress.result_data[index]['sentiment_positif']+progress.result_data[index]['sentiment_netral']+progress.result_data[index]['sentiment_negatif'])) * 100).toFixed(2),
        'percentage_negatif' : ((progress.result_data[index]['sentiment_negatif'] / (progress.result_data[index]['sentiment_positif']+progress.result_data[index]['sentiment_netral']+progress.result_data[index]['sentiment_negatif'])) * 100).toFixed(2)
      };
      newObj.topics.push(temp);
    }
    
    if (progress.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: progress.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
  
    
}

exports.getSample2019GantiPresiden = async (request, result, next) => {
    
  try {

    var sample = await ApiModel.getSampleTweet(5000, '2019GantiPresiden');   
    var newObj =  
    { 
      'sample_tweets' : sample.result_data
    }

    if (sample.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: sample.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
  
    
}
 
 

exports.getSample2019TetapJokowi = async (request, result, next) => {
    
  try {

    var sample = await ApiModel.getSampleTweet(5000, '2019TetapJokowi');   
    var newObj =  
    { 
      'sample_tweets' : sample.result_data
    }

    if (sample.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: sample.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
    
}
 


exports.getSampleJokowi = async (request, result, next) => {
    
  try {
    
    var sample = await ApiModel.getSampleTweet(5000, 'jokowi');   
    var newObj =  
    { 
      'sample_tweets' : sample.result_data
    }

    if (sample.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: sample.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
    
}
exports.getSamplePrabowo = async (request, result, next) => {
     
  try {
    
    var sample = await ApiModel.getSampleTweet(5000, 'prabowo');   
    var newObj =  
    { 
      'sample_tweets' : sample.result_data
    }

    if (sample.result_status) { 
      return result.json({ code : result.statusCode, success: true,  message: "OK", data: newObj });
    } else {  
      return result.json({ code : 500, success: false,  message: "Error Database", data: sample.result_data });
    }
  } catch (error) { 
    return result.json({ code : 500, success: false,  message: "Error Function", data: error.message });
  }
    
}
 

